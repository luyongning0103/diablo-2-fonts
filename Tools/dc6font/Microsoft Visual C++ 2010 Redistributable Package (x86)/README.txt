This version of dc6font.exe was compiled with Microsoft Visual C++ 2010, Express Edition.
If the msvcr100.dll is not already installed on your system, you have to install it prior to be using dc6font.

You can simply use the vcredist_x86.exe provided here, or you can use the original installer at : http://www.microsoft.com/download/en/details.aspx?id=5555
