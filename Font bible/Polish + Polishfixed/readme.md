Latin and latin2 both holds the same font but latin2 has a smaller Y gap, meaning the text is more condenced.  
In the fixed version, both latin and latin2 have the fixed font but you are free to use both. Please note that the bigger Y gap is better for readability.  

Polish fix changes :  
* Modified the 5 to disambiguate it from the 6
* Both Latin and Latin2 have the same dc6
* No changes made to the offset