# Installing the fonts for d2 mxl

## Step 1 : Download the font

Go to [This link](https://gitlab.com/Wasps/diablo-2-fonts/-/tree/master/Font%20bible) and pick your font.

The font files are the files ending with .dc6 and .tbl. Both are necessary to make the font works well.

## step 2 : Create the font's folder

Your diablo 2 main files are all located into .mpq files, but you can force D2 to use files from a data folder
Into your D2MXL install, create a data folder. In that data folder make a cascading folder of that manner : local/font/latin/
Your D2MXL install must now have that folder : data/local/font/latin/

Now copy the downloaded font files into the 'latin' folder and rename them both to 'font16'. Keep the extention as is.
The files must now be both called font16.tbl and font16.dc6.

## Step 3 : Force D2 to use those files

### For MedianXL users
Go into the launcher and press settings. In the lower right corner click on the 'application data' text.
You are now into the settings (and singleplayer character saves) of your launcher. Click the 'launcher' folder.

Quit the launcher now, and open the settings.json file with a text editor.

Modify this line ```"direct": false,``` to ```"direct": true,``` and save the file.

### For any other D2 users

Creat a shortcut of your D2 executable (Game.exe, Diablo2.exe). Right-click on that shortcut and click 'properties' then to the target add " -direct". the target must be then ' "C:\YOUR_GAME_PATH\Game.exe" -direct '

## Step 4 : ????

## Step 5 : Enjoy

launch the game via launcher or your shortcut to enjoy your new font.


## Misc

If you want to swap between fonts you have to delete the old fonts and place the new one with the correct name.

If you launch the game via game.exe, just make a shortcut and add -direct to the launching options.